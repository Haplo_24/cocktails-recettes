import { RouterModule, Routes } from '@angular/router';
import { PanierComponent } from "./panier.component";
import { NgModule } from '@angular/core';

const PANIER_ROUTE: Routes = [
  
  { path: '', component: PanierComponent },
]

@NgModule({
  imports: [RouterModule.forChild(PANIER_ROUTE)],
  exports: [RouterModule]
})
export class panierRouting { }