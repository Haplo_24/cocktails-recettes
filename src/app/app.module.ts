import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ActiveDirective } from './shared/directives/active.directive';
import { PanierComponent } from './panier/panier.component';
import { IngredientsListComponent } from './panier/ingredients-list/ingredients-list.component';
import { AppRoutingModule } from './app-routing';
import { PanierService } from './shared/services/panier.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CocktailModule } from './cocktails-container/cocktail.modules';
import { SharedModule } from './shared/modules/shared.module'
import { PanierModule } from './panier/panier.module';

@NgModule({
  declarations: [
    AppComponent,
    ActiveDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CocktailModule,
    SharedModule,
  ],
  providers: [PanierService],
  bootstrap: [AppComponent]
})
export class AppModule { }
