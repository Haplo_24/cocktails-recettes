import { Pipe, PipeTransform } from '@angular/core';
import { Cocktail } from '../models/cocktail.model';

@Pipe({
  name: 'filtre'
})
export class FiltrePipe implements PipeTransform {

  transform(cocktails:Cocktail[], recherche: string): Cocktail[] | null {
    if (!recherche.length) {
      return cocktails;
    } else {
      return cocktails.filter(cocktail => cocktail.name.toLowerCase().includes(recherche.toLowerCase()) );
    }
  }

}
