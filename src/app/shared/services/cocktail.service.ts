import { Injectable } from '@angular/core';
import { Cocktail } from '../models/cocktail.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

@Injectable()
export class CocktailService {

  public cocktails: BehaviorSubject<Cocktail[]> = new BehaviorSubject(null);
  
  constructor(private http: HttpClient) {
  //  this.http.put('https://cocktails-angular-369b3.firebaseio.com/cocktails-angular.json', this.cocktails.value).subscribe();
    this.cocktailsInit()
  }

  cocktailsInit(): void {
    this.http.get<Cocktail[]>('https://cocktails-angular-369b3.firebaseio.com/cocktails-angular.json').subscribe(cocktails => {
      this.cocktails.next(cocktails);
    })
  }

  getCocktail(index: string): Observable<Cocktail> {
    return this.cocktails.pipe(
      filter(cocktails => cocktails != null),
      map(cocktails => cocktails[index]))
  }

  addCocktail(cocktail: Cocktail) {
    const cocktails = this.cocktails.value;
    cocktails.push({name: cocktail.name, img: cocktail.img, desc: cocktail.desc, ingredients: cocktail.ingredients})
    this.cocktails.next(cocktails);
  }

  editCocktail(editCocktail: Cocktail) {
    const cocktails = this.cocktails.value.slice();
    const index = cocktails.map( c => c.name ).indexOf(editCocktail.name);
    cocktails[index] = editCocktail;
    this.cocktails.next(cocktails);
    this.save();
  }
  save(): void {
    this.http.put('https://cocktails-angular-369b3.firebaseio.com/cocktails-angular.json', this.cocktails.value).subscribe();
  }

}
