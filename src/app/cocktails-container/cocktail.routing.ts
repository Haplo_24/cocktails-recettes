import { RouterModule, Route } from '@angular/router';
import { CocktailDetailsComponent } from './cocktail-details/cocktail-details.component';
import { CocktailsContainerComponent } from './cocktails-container.component';
import { CocktailEditComponent } from './cocktail-edit/cocktail-edit.component';

const COCKTAIL_ROUTES: Route[] = [
  { path: 'cocktails', component: CocktailsContainerComponent, children: [
    {path: 'new', component: CocktailEditComponent },
    {path: ':index/edit', component: CocktailEditComponent },
    {path: ':index', component: CocktailDetailsComponent },
    {path: '', component: CocktailDetailsComponent }
  ]}
]

export const cocktailRouting = RouterModule.forChild(COCKTAIL_ROUTES)