import { NgModule } from "@angular/core";
import { CocktailsListComponent } from './cocktails-list/cocktails-list.component';
import { CocktailDetailsComponent } from './cocktail-details/cocktail-details.component';
import { CocktailsContainerComponent } from './cocktails-container.component';
import { CocktailEditComponent } from './cocktail-edit/cocktail-edit.component';
import { FiltrePipe } from '../shared/pipes/filtre.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { cocktailRouting } from './cocktail.routing';
import { SharedModule } from '../shared/modules/shared.module';

@NgModule({
  declarations:[
    CocktailsListComponent,
    CocktailDetailsComponent,
    CocktailsContainerComponent,
    CocktailEditComponent,
    FiltrePipe
  ],
  imports:[
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    cocktailRouting,
    SharedModule
  ],
  providers:[],
  exports:[]
})

export class CocktailModule {}